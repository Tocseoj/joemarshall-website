import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

// Auto register components (globally) based on file name
const files = require.context("./", true, /\.vue$/i);
files.keys().map(key =>
  Vue.component(
    key
      .split("/")
      .pop()
      .split(".")[0],
    files(key).default
  )
);

new Vue({
  render: h => h(App)
}).$mount("#app");
